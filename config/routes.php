<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

use Hyperf\HttpServer\Router\Router;

Router::addRoute(['GET', 'POST', 'HEAD'], '/', 'App\Controller\Http\Index@index');
Router::addRoute(['GET'], '/api/get_uid', 'App\Controller\Http\Index@getUid');
// 该 Group 下的所有路由都将应用配置的中间件
Router::addGroup(
    '/api/push', function () {
    	// 全站推送消息
    	// {"data":{"msg":{"msg":"测试","uid":"12345"}},"token":"123456"}
        Router::post('/all', [\App\Controller\Http\Api\Push::class, 'all']);
        // 指定uid推送，支持群发
    	// {"data":{"msg":{"msg":"测试","uid":"12345"},"uids":["123456"]},"token":"令牌"}
        Router::post('/uids', [\App\Controller\Http\Api\Push::class, 'uids']);
        // 集合推送，支持不同uid推送不同信息
    	// {token:'令牌验证',data:{list:[{uid:'uid1','msg':'推送内容'}]}}
        Router::post('/list', [\App\Controller\Http\Api\Push::class, 'list']);
    },
    ['middleware' => [\App\Middleware\Auth\Manage::class]]
);

// 该 Group 下的所有路由都将应用配置的中间件
Router::addGroup(
    '/api/manage_connections', function () {
    	// 强制断开全站连接
    	//  {"data":{},"token":"令牌"}
        Router::post('/close_all', [\App\Controller\Http\Api\ManageConnections::class, 'closeAll']);
        // 根据连接fd 强制断开连接，需先根据用户id 到getUidFd获取到fd信息
    	//  {"data":{"fds":["1"]},"token":"令牌"}
        Router::post('/close_fds', [\App\Controller\Http\Api\ManageConnections::class, 'closeFds']);
        // 根据uid 断开连接，支持多uid
    	//  {"data":{"uids":["123456"]},"token":"令牌"}
        Router::post('/close_uids', [\App\Controller\Http\Api\ManageConnections::class, 'closeUids']);
      	// 根据用户uid获取连接fd
    	// {"data":{"uids":["123456"]},"token":"令牌"}
        Router::post('/uid_fds', [\App\Controller\Http\Api\ManageConnections::class, 'uidFds']);
                //获取全部在线uid
        // {"data":{},"token":"令牌"}
        Router::post('/uids', [\App\Controller\Http\Api\ManageConnections::class, 'uids']);
    },
    ['middleware' => [\App\Middleware\Auth\Manage::class]]
);
// 该 Group 下的所有路由都将应用配置的中间件
Router::addGroup(
    '/api/abnormal', function () {
    	// 获取异常ip
    	//  {"data":{"page":"1"},"token":"令牌"}
        Router::post('/ips', [\App\Controller\Http\Api\Abnormal::class, 'ips']);
        // 获取异常ip的详细数据
    	//  {"data":{"ip":"117.0.1.1"},"token":"令牌"}
        Router::post('/ip_info', [\App\Controller\Http\Api\Abnormal::class, 'ipInfo']);
        // 删除ip 支持多ip
    	//  {"data":{"ips":["117.0.1.1"]},"token":"令牌"}
        Router::post('/delete_ips', [\App\Controller\Http\Api\Abnormal::class, 'deleteIps']);
    //     // 根据uid 断开连接，支持多uid
    // 	//  {"data":{"uids":["123456"]},"token":"令牌"}
    //     Router::post('/close_uids', [\App\Controller\Http\Api\Abnormal::class, 'closeUids']);
    //   	// 根据用户uid获取连接fd
    // 	// {"data":{"uids":["123456"]},"token":"令牌"}
    //     Router::post('/get_uid_fds', [\App\Controller\Http\Api\Abnormal::class, 'getUidFds']);
    },
    ['middleware' => [\App\Middleware\Auth\Manage::class]]
);
Router::addServer('ws', function () {
    Router::get('/', 'App\Controller\Websocket\Index');
});
Router::addServer('wss', function () {
    Router::get('/', 'App\Controller\Websocket\Index');
});