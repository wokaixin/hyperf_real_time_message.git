<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

return [
    	'getUid' => [
    		'config'=>[
		        'base_uri' => 'http://192.168.205.1:8080/renren-fast',
		        'method'=>'GET',
		        'timeout' => 5,
		        'swoole' => [
		            'timeout' => 10,
		            'socket_buffer_size' => 1024 * 1024 * 2,
		        ]
		    ],"params"=>['token'=>'123456'],"route"=>"/notice/user/uid"
        ]
];
