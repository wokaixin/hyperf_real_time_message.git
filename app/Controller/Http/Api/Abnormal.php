<?php
declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

namespace App\Controller\Http\Api;
use Hyperf\Server\ServerFactory;
use Hyperf\Redis\RedisFactory;
use Hyperf\Utils\ApplicationContext;
use App\Model\User;
class Abnormal extends \App\Controller\Http\Base {
    protected $quantity=50;//查询ip每页数量
        // 获取异常ip
    	//  {"data":{"page":"1"},"token":"令牌"}
        public function ips(){
            $num=$this->quantity;
            $params = $this->request->all();
            $data=$params['data'];
            if (isset($data['page']) ) {
        
                $container = ApplicationContext::getContainer();
                $redisabnormalIp = $container->get(RedisFactory::class)->get('abnormalIp');
                $iplist=$redisabnormalIp->keys('*');
                $page=isset($data['page'])??1;
                $page=intval($page);
                $start=($page-1)*$num;
                $end=$page*$num;
                // $list=array_slice($iplist,$start,$end);
                $data=[];
                foreach($iplist as $k=>$v){
                    if($k>$end){
                        break;
                    }elseif($k>=$start){
                        $data[$v]=['count'=>$redisabnormalIp->lLen($v)??1];   
                    }
                }
                
                return[
                        'code'=>0,
                        'count'=>count($iplist),
                        'list'=>json_encode($data)
                    ];
            }
            return ['code'=>1,'fail'=>'数据格式有误'];
        }
        
        // 获取异常ip的详细数据
    	//  {"data":{"ip":"117.0.1.1"},"token":"令牌"}
        public function  ipInfo(){
            $params = $this->request->all();
            $data=$params['data'];
            $num=0;
            if (isset($data['ip'])) {
        
                $container = ApplicationContext::getContainer();
                $redis = $container->get(RedisFactory::class)->get('abnormalIp');
                $data=$redis->lrange($data['ip'], 0 ,-1);
                if($data){
                    return[
                        'code'=>0,
                        'list'=>$data,
                        'count'=>count($data),
                        'message'=>'删除成功',
                    ];
                }
            }
            return ['code'=>1,'fail'=>'数据格式有误'];
        }
        
        // 删除ip 支持多ip
    	//  {"data":{"ips":["117.0.1.1"]},"token":"令牌"}
        public function deleteIps(){

            $params = $this->request->all();
            $data=$params['data'];
            $num=0;
            if (isset($data['ips']) && is_array($data['ips'])) {
        
                $container = ApplicationContext::getContainer();
                $redisabnormalIp = $container->get(RedisFactory::class)->get('abnormalIp');
                $iplist=$redisabnormalIp->keys('*');
                $ips=$data['ips'];
                foreach($ips as $k=>$v){

                    if($redisabnormalIp->del($v)){
                        $num++;
                    }

                }
                
                return[
                        'code'=>0,
                        'count'=>$num,
                        'message'=>'删除成功',
                    ];
            }
            return ['code'=>1,'fail'=>'数据格式有误'];
        }
}