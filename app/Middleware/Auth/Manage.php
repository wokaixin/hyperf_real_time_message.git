<?php

declare(strict_types=1);

namespace App\Middleware\Auth;

use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface as HttpResponse;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class Manage implements MiddlewareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var HttpResponse
     */
    protected $response;

    public function __construct(ContainerInterface $container, HttpResponse $response, RequestInterface $request)
    {
        $this->container = $container;
        $this->response = $response;
        $this->request = $request;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        // 根据具体业务判断逻辑走向，这里假设用户携带的token有效
        // $isValidToken = true;
        // if ($isValidToken) {
        //     return $handler->handle($request);
        // }

        $params = $this->request->all();
        if (is_array($params) && $params['token'] && $params['token']==='123456') {
            $data=$params['data']??null;
            if (!is_array($data)) {
                return $this->response->json(
                    [
                        'code' => 1,
                        'fail'=> '数据格式有误',
                        
                    ]);
            }
            return $handler->handle($request);
        }
                return $this->response->json(
                    [
                        'code' => -1,
                        'fail' => '验证token无效，阻止继续向下执行',
                    ]
                );
        
    }
}